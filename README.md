# Voron Trident Klipper Configuration

This repository contains the klipper configuration for a 250mm Voron Trident:

* Btt Octopus 1.1
* Voron Tap
* Stealthburner
* Btt Sbb2209 rp2040
* Dual Bed temperature sensors
* Chamber temperature sensor


Printed Mods:

* Nozzle Scrubber
    * https://www.printables.com/model/298565-nozzle-scrubber-arm-extension-voron-trident/files
    * https://www.printables.com/model/201999-nozzle-scrubber-with-a-little-bucket-for-voron-24
* Canbus cable brace
    * https://github.com/bigtreetech/EBB/blob/master/EBB%20SB2240_2209%20CAN/STL/Printed_Part_for_CAN_Cable_V1.2.stl
    * https://github.com/bigtreetech/EBB/blob/master/EBB%20SB2240_2209%20CAN/STL/CW2%20Cable%20Bridge.STL
* Filament scrubber and Guide
    * https://www.printables.com/model/335418-filament-scrubber-and-guide
* Panel Latches
    * https://mods.vorondesign.com/details/9Rdnf5vD2oaJLmR7BpAuQ
* Handles
    * https://mods.vorondesign.com/details/EAM1ZiQJCUzXznvOA767w
* Spool Holder
    * https://www.printables.com/model/500221-voron-spool-holder-608-bearings
